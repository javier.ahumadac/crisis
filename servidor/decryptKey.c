#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#define LEN 32

char key[LEN];
void get(){
    // PARA MANDAR TEMPERATURAS Y KEYS CIFRADOS
    system("git pull origin main");
}
void getKey(){
    FILE *k;
    k = fopen("../key", "w");
    int i;
    for (i=0; i<LEN; i++){
        fprintf(k,"%c",key[i]);
        srand(rand());
    }
    fclose(k);
}

// ASCII https://www.cprograms4future.com/p/
void asciiDecrypt(int num[LEN]){
    //Prints ascii value of given string
    int i;
    printf("Decrypted ASCII code\n");
    for(i=0; i<LEN; i++) {  
        key[i] = (char) num[i];
    }
    for(i=0; i<LEN; i++) {  
        printf("%c", key[i]);
    }
    printf("\n");
    getKey();
}



int main(){
    get();
    char KEY[100];
    int dec[LEN];
    

    FILE *Key_enc;
    Key_enc = fopen("../key", "r");

    int i;
    for (i = 0; i < LEN; i++){
        fscanf(Key_enc, "%d", &dec[i] );
    }

    fclose(Key_enc);

    for (i=0; i<LEN; i++){
        printf("%d ", dec[i]);
    }
    
    asciiDecrypt(dec);

    return 0;
}
